Test Accuracy:

|  | Fully Connected | ConvNet |
| ------ | ------ | ------ |
| Mnist   | 0.9689 | 0.9879 |
| Fashion Mnist | 0.8784 | 0.9135 |
